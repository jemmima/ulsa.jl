__precompile__()
module ULSA

using Pkg
using CSV
using DataFrames
using JLD
using FileIO
using Statistics
using LinearAlgebra
using HDF5
using ProgressBars
using DecisionTree
using BenchmarkTools

include("Identification.jl")
include("PeakFind.jl")
include("Screening.jl")


export featureMF_comp, featureID_comp, featureID_comp_batch, featureID_ext, featureID_comp_ExDB, suslist2entries, proteine_screening_ms1, suspect_screening

###############################


#######################################################


end # end of the module


#################################################
# test area 
 """

mode = "POSITIVE"
source = "ESI"
path2comp = "/Users/saersamanipour/Downloads/20180308-pos-S11D6-EFF-A_report_comp.csv"

weight_f =[1,1,1,1,1,1,1]

# featureID_comp(mode,source,path2comp,weight_f)

"""